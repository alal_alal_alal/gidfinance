<?php


use LeadGenerator\Lead;
use function parallel\run;

class Worker
{

    const  COUNT = 10000;

    /**
     * @var $leads array
     */
    public $leads;
    /**
     * @var   $workers_count int
     * @descr count of the workers
     */
    public $workers_count = 8;

    /**
     * @param $executor
     */
    public function run($executor)
    {

        $gn = new  \LeadGenerator\Generator();
        $gn->generateLeads(Worker::COUNT, [$this, $executor]);
        $this->execute_queue();


    }

    /**
     * @param array $headers
     * @param array $rows
     * @return false
     */
    public function table($headers = [], $rows = [])
    {
        if (!class_exists('cli\Table')) {
            return false;
        }
        $table = new  cli\Table($headers, $rows);
        $table->display();

    }

    /**
     * @throws Exception
     */
    public function initialize()
    {

        $exists = file_exists(Config::FILE);
        $count = $exists ? $count = shell_exec('wc ' . Config::FILE) : false;
        $is_writable = is_writeable(Config::FILE);
        if ($exists && $count) {
            list  ($lines, $words, $characters, $file) = ($values = explode(' ', trim($count)));
            $this->table(['lines', 'words', 'bytes'], [[$lines, $words, $characters]]);
        }
        if ($is_writable) {
            $choice = $this->read_sdtdin(Config::FILE . ' already  exists  and has  ' . $count . 'lines. cleat It? ', array('', 'no'), '', 's');

            if ($choice == '') {

                $ok = file_put_contents(Config::FILE, '');
                if ($ok === false) {
                    throw  new  Exception('not available for writing');
                } else {
                    $choice = $this->read_sdtdin(Config::FILE . ' file cleaned up successfully, type [enter]  to  process queue ', ['']);
                }
            } else {

                $choice = $this->read_sdtdin('type [enter]  to  process queue ', ['']);
            }


        } else {
            $choice = $this->read_sdtdin(Config::FILE . '  not available for writing. can\'t create file! please create a file and grant write permissions ', array('', 'exit'), '', 'e');
            die('bye');
        }
    }

    /**
     * @param  $str
     * @param string $type
     * @return mixed|string
     */
    public function colorize($str, $type = 'i')
    {
        switch ($type) {
            case 'e': //error
                return "\033[31m$str \033[0m";
                break;
            case 's': //success
                return "\033[32m$str \033[0m";
                break;
            case 'w': //warning
                return "\033[33m$str \033[0m";
                break;
            case 'i': //info
                return "\033[36m$str \033[0m";
                break;
            default:
                return $str;
                break;
        }
    }

    /**
     * @param  $prompt
     * @param array $valid_inputs
     * @param string $default
     * @param false $color
     * @return mixed|string
     */
    public function read_sdtdin(string $prompt, array $valid_inputs = [], string $default = '', string $color = '', string $ln = PHP_EOL)
    {
        while (!isset($input) || (is_array($valid_inputs) && !in_array($input, $valid_inputs)) || ($valid_inputs == 'is_file' && !is_file($input))) {
            echo ($color ? $this->colorize($prompt, $color) : $prompt) . (is_array($valid_inputs) && count($valid_inputs) ? '[' . $this->colorize(implode(',', preg_replace('/^$/', 'Enter', $valid_inputs))) . ']' : '') . $ln;
            $input = strtolower(trim(fgets(STDIN)));
            if (empty($input) && !empty($default)) {
                $input = $default;
            }
        }
        return $input;
    }

    /**
     * @param $lead
     */
    public function add_to_queue(Lead $lead)
    {
        $this->leads[$lead->id] = $lead;
    }

    /**
     *
     */
    public function execute_queue()
    {
        $producer = function (int $id, string $categoryName = null) {

            include_once __DIR__ . '/Config.php';

            sleep(Config::WORK_TIME);
            Config::debug(' [' . $id . '] processed');
            sleep(2);
            $fp = fopen(realpath(Config::FILE), 'a');
            $str = implode('|', [$id, $categoryName, date('d.m.Y H:i:s')]);

            if (flock($fp, LOCK_EX)) {
                $ok = fwrite($fp, $str . "\n");
                $ok = $ok && fflush($fp);
                flock($fp, LOCK_UN);
            } else {

                Config::debug(' [' . $id . '] ' . "Failed to get the LOCK_EX");
            }
            fclose($fp);

            Config::debug(' {' . $id . '} finished ' . ($ok ? 'OK' : 'ERROR'));

        };
        foreach (array_chunk($this->leads, $this->workers_count) as $chunk) {
            foreach ($chunk as $lead) {
                Config::debug(' (' . $lead->id . ') added');
                $futures[] = run($producer, [($lead->id), ($lead->categoryName)]);


            }
        }
    }


}
