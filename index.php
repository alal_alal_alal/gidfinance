<?php
require 'lead-generator/src/Generator.php';
require 'lead-generator/src/Lead.php';
require 'Config.php';
require 'Worker.php';
require './vendor/autoload.php';

$app = new Worker();
$app->initialize();
$app->run('add_to_queue');