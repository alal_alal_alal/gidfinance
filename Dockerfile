FROM php:7.4-cli
RUN apt-get install  mc -y
RUN apt-get install  git -y
RUN apt-get install  nano -y
RUN apt-get update && apt-get install -y
RUN pecl install redis-5.1.1 \
    && pecl install xdebug-2.8.1 \
    && docker-php-ext-enable redis xdebug
RUN docker-php-source extract \
    # do important things \
    && docker-php-source delete

    RUN cd  /var/www/html/gidfinance# git
      RUN clone https://gitlab.com/alal_alal_alal/gidfinance.git
