<?php


class Config
{

    /**
     * @descr $deug  -  show  messages  to  stdout
     */

    const DEBUG =  true;
    const FILE = 'log.txt';
    const  WORK_TIME = 2;/* for  sleep ()*/

    public static function debug(string $string,  $eol =  true)
    {
        if (Config::DEBUG) {
            echo $string.($eol?PHP_EOL:'');
        }
    }

}
